import { createRouter, createWebHistory } from "vue-router";
import Home from "../view/Home.vue";
import Person from '../view/Person.vue'
import Creation from '../view/Creation.vue';
import Login from '../view/Login.vue';

const routes = [
    {
      path: "/",
      name: "Home",
      component: Home,
    },
    {
      path: "/person",
      name: "Person",
      component: Person,
    },
    {
      path: "/create",
      name: "Creation",
      component: Creation,
    },
    {
      path: "/login",
      name: "Login",
      component: Login,
    }
  ];
  
  const router = createRouter({
    history: createWebHistory(),
    routes,
  });
  
  export default router;